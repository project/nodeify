<?php

namespace Drupal\nodeify\Form;

use Drupal\node\NodeTypeInterface;
use Drupal\nodeify\TokenInfoTrait;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class MessageSettingsForm extends ConfigFormBase {

  use TokenInfoTrait;

  /**
   * @var EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nodeify.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nodeify_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    parent::__construct($config_factory);

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  private function getActions() {
    return [
      'create' => 'Create: <em>@node_type</em>',
      'update' => 'Update: <em>@node_type</em>',
      'delete' => 'Delete: <em>@node_type</em>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $entity_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
    foreach ($entity_types as $node_type_id => $node_type) {
      $form[$node_type_id] = [
        '#title' => $node_type->label(),
        '#type' => 'details',
        '#tree' => TRUE,
      ];

      $this->buildActionsForm($form[$node_type_id], $node_type, [$node_type->id()]);
    }
    $form_state->setTemporaryValue('entity_types', $entity_types);
    return parent::buildForm($form, $form_state);
  }

  /**
   * Get the entity type config form for each action.
   *
   * @param array $element
   *   The base element to add to.
   * @param NodeTypeInterface $node_type
   *   The current node type to pull config from.
   * @param array $parents
   *   An array of form parents for #states use.
   */
  public function buildActionsForm(array &$element, NodeTypeInterface $node_type, array $parents = []) {
    foreach ($this->getActions() as $action => $action_label) {
      $config = $node_type->getThirdPartySetting('nodeify', $action);
      $element[$action] = [
        '#type' => 'fieldset',
        '#title' => $this->t($action_label, ['@node_type' => $node_type->label()]),
        '#open' => TRUE,
      ];
      $element[$action]['status'] = [
        '#type' => 'radios',
        '#title' => $this->t('Status'),
        '#title_display' => 'invisible',
        '#options' => [
          'disable' => $this->t('Disable'),
          'enable' => $this->t('Enable'),
          'suppress' => $this->t('Suppress')
        ],
        '#default_value' => $config['status'] ?? 'disable',
      ];
      $input_key = $this->getStatesInputKey($parents, [$action, 'status']);
      $element[$action]['text'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Message text'),
        '#default_value' => $config['text'] ?? '',
        '#states' => [
          'visible' => [
            $input_key => ['value' => 'enable']
          ],
        ],
      ];

      $element[$action]['text_tokens'] = $this->getTokenInfoList();
      $element[$action]['text_tokens']['#states'] = [
        'visible' => [
          $input_key => ['value' => 'enable']
        ],
      ];

      // Only open it if one of the actions have an override.
      if (!empty($config['status']) && $config['status'] !== 'disable' && !isset($element['#open'])) {
        $element['#open'] = TRUE;
      }
    }
  }

  private function getStatesInputKey(...$args) {
    $parents = array_merge(...$args);
    $root = array_shift($parents);
    $key = '[' . join('][', $parents) . ']';
    return ":input[name=\"{$root}{$key}\"]";
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $node_types = $form_state->getTemporaryValue('entity_types');
    foreach ($form_state->cleanValues()->getValues() as $bundle => $bundle_config) {
      foreach ($bundle_config as $action => $action_config) {
        $node_types[$bundle]->setThirdPartySetting('nodeify', $action, $action_config);
      }
      $node_types[$bundle]->save();
    }
    parent::submitForm($form, $form_state);
  }

}
