<?php

namespace Drupal\nodeify;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MessageHandler implements ContainerAwareInterface {

  use ContainerAwareTrait;

  use DependencySerializationTrait;

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $tokenUtility;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(Token $token_utility, EntityTypeManagerInterface $entity_type_manager, MessengerInterface $messenger, RendererInterface $renderer) {
    $this->tokenUtility = $token_utility;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
    $this->renderer = $renderer;
  }

  /**
   * Adds message submit handlers to node forms including node delete confirm from.
   *
   * @see nodeify_form_node_form_altero()
   * @see nodeify_form_node_confirm_form_alter()
   */
  public function addSubmitHandlers(&$form, FormStateInterface $form_state) {
    $node = $form_state->getFormObject()->getEntity();
    $node_type = $this->entityTypeManager->getStorage('node_type')->load($node->bundle());
    if (!$config = $node_type->getThirdPartySettings('nodeify')) {
      return;
    }
    if ($this->isActionable($config, 'create') && $node->isNew() && $form_state->getFormObject()->getFormId() == "node_{$node->bundle()}_form") {
      $form['actions']['submit']['#submit'][] = [$this, 'nodeFormCreateSubmit'];
    }
    if ($this->isActionable($config, 'update') && $form_state->getFormObject()->getFormId() == "node_{$node->bundle()}_edit_form") {
      $form['actions']['submit']['#submit'][] = [$this, 'nodeFormUpdateSubmit'];
    }
    if ($this->isActionable($config, 'delete') && $form_state->getFormObject()->getFormId() == "node_{$node->bundle()}_delete_form") {
      $form['actions']['submit']['#submit'][] = [$this, 'nodeFormDeleteSubmit'];
    }
  }

  private function isActionable(array $config, $action) {
    if (!isset($config[$action]['status'])) {
      return FALSE;
    }
    return $config[$action]['status'] === 'enable' || $config[$action]['status'] === 'suppress';
  }

  /**
   * Perform the override to current drupal status messages.
   *
   * @param $pattern
   *   The current message that should be removed.
   * @param null $replacement
   *   The new message, null if we just want to suppress a message.
   */
  public function replace($pattern, $replacement = NULL) {
    $statuses = $this->messenger->deleteByType('status');

    $filtered = array_filter($statuses, function ($message) use ($pattern) {
      return preg_match($pattern, $message) !== 1;
    });

    foreach ($filtered as $message) {
      $this->messenger->addStatus($message);
    }

    if ($replacement) {
      $this->messenger->addStatus(new FormattableMarkup($replacement, []));
    }
  }

  /**
   * Perform submit callback on submitted forms.
   *
   * @param $form
   * @param FormStateInterface $form_state
   * @param $action
   *   The action which matches the config definition.
   * @param $action_message_text
   *   The action which matches the actual text replacement in the core message.
   */
  public function processForm(&$form, FormStateInterface $form_state, $action, $action_message_text) {
    $node = $form_state->getFormObject()->getEntity();
    $node_type = $this->entityTypeManager->getStorage('node_type')->load($node->bundle());
    $node_type_label = $node_type->label();

    $pattern = "/^(The\ )?$node_type_label.*has been $action_message_text/";

    $config = $node_type->getThirdPartySetting('nodeify', $action, []);
    if ($config['status'] === 'suppress') {
      $this->replace($pattern);
      return;
    }

    $replacement_text = $config['text'] ?? '';
    $data = [
      'node' => $node,
    ];
    foreach ($node->getFields() as $field_name => $field) {
      if (!empty($field->entity)) {
        $entity_type = $field->entity->getEntityTypeId();
        $bundle = $field->entity->bundle();
        // We'll figure out a more efficient way to do this later, it's not needed right now.
        $key = $bundle == $entity_type ? "node_{$entity_type}" : "{$entity_type}_{$bundle}";
        $data[$key] = $field->entity;
      }
    }
    $this->replace($pattern, $this->process($replacement_text, $data));
  }

  /**
   * Custom form #submit handler.
   *
   * @param $form
   * @param FormStateInterface $form_state
   */
  public function nodeFormDeleteSubmit(&$form, FormStateInterface $form_state) {
    $this->processForm($form, $form_state, 'delete', 'deleted');
  }

  /**
   * Custom form #submit handler.
   *
   * @param $form
   * @param FormStateInterface $form_state
   */
  public function nodeFormUpdateSubmit(&$form, FormStateInterface $form_state) {
    $this->processForm($form, $form_state, 'update', 'updated');
  }

  /**
   * Custom form #submit handler.
   *
   * @param $form
   * @param FormStateInterface $form_state
   */
  public function nodeFormCreateSubmit(&$form, FormStateInterface $form_state) {
    $this->processForm($form, $form_state, 'create', 'created');
  }

  /**
   * Perform placeholder replacements on text.
   *
   * @param $text
   *   A string with either token module tokens or twig tokens.
   * @param array $data
   *   An array of objects to be used for tokens or as twig context.
   *
   * @return mixed
   */
  protected function process($text, $data = []) {
    $element = [
      '#type' => 'inline_template',
      '#template' => $text,
      '#context' => $data,
    ];
    $text = $this->renderer->render($element);
    return $this->tokenUtility->replace($text->__toString(), $data);
  }
}
