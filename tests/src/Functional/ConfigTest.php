<?php

namespace Drupal\Tests\nodeify\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\node\NodeInterface;
use Drupal\Tests\node\Functional\NodeTestBase;

/**
 * Tests configuration pages for nodeify.
 *
 * @group nodeify
 */
class ConfigTest extends NodeTestBase {

  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  public static $modules = [
    'nodeify',
    'field_ui',
    'block'
  ];

  public static $messageText = [
    'nodeify[create]' => 'New page created: "[node:title]".',
    'nodeify[update]' => '[node:title] has been updated.',
    'nodeify[delete]' => '[node:title] deleted.',
  ];

  public static $userPermissions = [
    'bypass node access', 'administer content types', 'administer node fields'
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->drupalPlaceBlock('system_breadcrumb_block');
  }

  /**
   * Tests access without administer nodeify permission.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testUnauthorizedUser() {
    $assert_session = $this->assertSession();
    $permissions = self::$userPermissions;
    // User without specific permissions.
    $web_user = $this->drupalCreateUser($permissions);
    $this->drupalLogin($web_user);

    $this->drupalGet('admin/structure/types/manage/page');
    $assert_session->pageTextNotContains('Message settings');
  }

  /**
   * Adds configuration settings and tests node create/update/delete.
   */
  public function testNodeTypeEditForm() {
    $assert_session = $this->assertSession();
    $permissions = self::$userPermissions;
    $permissions[] = 'administer nodeify';
    // User with the correct permission.
    $web_user = $this->drupalCreateUser($permissions);
    $this->drupalLogin($web_user);
    $this->drupalGet('admin/structure/types/manage/page');
    $assert_session->pageTextContains('Message settings');

    $this->submitForm(self::$messageText, t('Save content type'));

    $edit = [
      'title[0][value]' => 'Test page',
      'body[0][value]' => 'Test body text',
    ];
    $this->drupalGet('node/add/page');
    $this->submitForm($edit, t('Save'));

    $node = $this->getSavedNode([
      'type' => 'page',
      'title' => 'Test page',
    ]);

    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains($this->getMessageText('create', $node));
    $assert_session->pageTextNotContains($this->getDefaultMessageText('create', $node));

    $this->drupalGet('node/' . $node->id() . '/edit');
    $this->submitForm([], t('Save'));
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains($this->getMessageText('update', $node));
    $assert_session->pageTextNotContains($this->getDefaultMessageText('update', $node));

    $this->drupalGet('node/' . $node->id() . '/delete');
    $this->submitForm([], t('Delete'));
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContains($this->getMessageText('delete', $node));
    $assert_session->pageTextNotContains($this->getDefaultMessageText('delete', $node));

    // Save settings on the other configuration form.
    $new_create_message = 'Your basic page: [node:title], has been created.';

    $this->drupalGet('admin/config/system/nodeify');
    $this->submitForm([
      'page[create]' => $new_create_message,
    ], 'Save configuration');

    $edit = [
      'title[0][value]' => 'Test page 2',
      'body[0][value]' => 'Test body text 2',
    ];
    $this->drupalGet('node/add/page');
    $this->submitForm($edit, t('Save'));

    $assert_session->statusCodeEquals(200);

    $node = $this->getSavedNode([
      'type' => 'page',
      'title' => 'Test page 2',
    ]);

    $assert_session->pageTextContains($this->replaceTitleToken($new_create_message, $node));
  }

  /**
   * Fetches a particular node that has been saved by drupalPostForm.
   *
   * @param array $properties
   *
   * @return \Drupal\node\NodeInterface
   */
  protected function getSavedNode(array $properties) {
    /** @var \Drupal\Node\NodeStorageInterface $node_storage */
    $node_storage = $this->container->get('entity_type.manager')->getStorage('node');
    // Verify forum.
    $nodes = $node_storage->loadByProperties($properties);
    /** @var \Drupal\node\NodeInterface $node */
    $node = array_shift($nodes);
    return $node;
  }

  /**
   * Gets message text from settings.
   *
   * @param $op
   * @param \Drupal\node\NodeInterface|NULL $node
   *
   * @return mixed
   */
  protected function getMessageText($op, NodeInterface $node = NULL) {
    $message_text = self::$messageText;
    $text  = $message_text["nodeify[$op]"];
    if ($node) {
      $text = $this->replaceTitleToken($text, $node);
    }
    return $text;
  }

  /**
   * Forms the default text that Drupal core provides for node actions.
   *
   * @param $op
   * @param \Drupal\node\NodeInterface $node
   *
   * @return \Drupal\Component\Render\FormattableMarkup
   */
  protected function getDefaultMessageText($op, NodeInterface $node) {
    switch ($op) {
      case 'create':
        $text = 'Basic page @title has been created.';
        break;
      case 'update':
        $text = 'Basic page @title has been updated.';
        break;
      case 'delete':
        $text = 'The Basic page @title has been deleted.';
        break;
    }

    return new FormattableMarkup($text, [
      '@title' => $node->label(),
    ]);
  }

  /**
   * Replaces the title token for testing.
   *
   * @param $text
   * @param \Drupal\node\NodeInterface $node
   *
   * @return mixed
   */
  protected function replaceTitleToken($text, NodeInterface $node) {
    return str_replace('[node:title]', $node->label(), $text);
  }
}
